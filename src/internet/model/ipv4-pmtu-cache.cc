/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2022 Jadavpur University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * Author: Tommaso Pecorella <tommaso.pecorella@unifi.it>
 * Author: Akash Mondal <a98mondal@gmail.com>
 */

#include "ipv4-pmtu-cache.h"
#include "ns3/log.h"
#include "ns3/simulator.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Ipv4PmtuCache");

NS_OBJECT_ENSURE_REGISTERED (Ipv4PmtuCache);

TypeId Ipv4PmtuCache::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::Ipv4PmtuCache")
    .SetParent<Object> ()
    .SetGroupName ("Internet")
    .AddAttribute ("CacheExpiryTime",
                   "Validity time for a Path MTU entry. Default is 10 minutes, minimum is 5 minutes.",
                   TimeValue (Seconds (60 * 10)),
                   MakeTimeAccessor (&Ipv4PmtuCache::m_validityTime),
                   MakeTimeChecker (Time (Seconds (60 * 5))))
  ;
  return tid;
}

Ipv4PmtuCache::Ipv4PmtuCache ()
{
}

Ipv4PmtuCache::~Ipv4PmtuCache ()
{
}

void Ipv4PmtuCache::DoDispose ()
{
  for (pathMtuTimerIter iter = m_pathMtuTimer.begin (); iter != m_pathMtuTimer.end (); iter++)
    {
      iter->second.Cancel ();
    }
  m_pathMtuTimer.clear ();
  m_pathMtu.clear ();
  m_pmtuChangeCallback.Nullify ();
}

uint32_t Ipv4PmtuCache::GetPmtu (Ipv4Address dst)
{
  NS_LOG_FUNCTION (this << dst);

  if (m_pathMtu.find (dst) != m_pathMtu.end ())
    {
      return m_pathMtu[dst];
    }
  return 0;
}

void Ipv4PmtuCache::SetPmtu (Ipv4Address dst, uint32_t pmtu)
{
  NS_LOG_FUNCTION (this << dst << pmtu);

  m_pathMtu[dst] = pmtu;
  m_pmtuChangeCallback(dst, pmtu);
  if (m_pathMtuTimer.find (dst) != m_pathMtuTimer.end ())
    {
      m_pathMtuTimer[dst].Cancel ();
    }
  EventId pMtuTimer;
  pMtuTimer = Simulator::Schedule (m_validityTime, &Ipv4PmtuCache::ClearPmtu, this, dst);
  m_pathMtuTimer[dst] = pMtuTimer;
}

Time Ipv4PmtuCache::GetPmtuValidityTime () const
{
  NS_LOG_FUNCTION (this);
  return m_validityTime;
}

bool Ipv4PmtuCache::SetPmtuValidityTime (Time validity)
{
  NS_LOG_FUNCTION (this << validity);

  if (validity > Seconds (60 * 5))
    {
      m_validityTime = validity;
      return true;
    }

  NS_LOG_LOGIC ("rejecting a PMTU validity timer lesser than 5 minutes");
  return false;
}

void Ipv4PmtuCache::ClearPmtu (Ipv4Address dst)
{
  NS_LOG_FUNCTION (this << dst);

  m_pathMtu.erase (dst);
  m_pathMtuTimer.erase (dst);
  m_pmtuChangeCallback(dst, 0);
}

void Ipv4PmtuCache::SetPmtuChangeCallback (PmtuChangeCallback callback)
{
  NS_LOG_FUNCTION (this << &callback);
  
  m_pmtuChangeCallback = callback;
}

}

