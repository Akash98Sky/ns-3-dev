/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2022 Jadavpur University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * Author: Tommaso Pecorella <tommaso.pecorella@unifi.it>
 * Author: Akash Mondal <a98mondal@gmail.com>
 */

#ifndef IPV4_PMTU_CACHE_H
#define IPV4_PMTU_CACHE_H

#include <map>

#include "ns3/object.h"
#include "ns3/nstime.h"
#include "ns3/type-id.h"
#include "ns3/event-id.h"
#include "ns3/ipv4-address.h"

namespace ns3 {

/**
 * \ingroup ipv4
 *
 * \brief This class implements the Path MTU cache, as defined by \RFC{1191}.
 *
 * The Path MTU is stored according to the destination address, and it is
 * cleared upon expiration (default validity time is 10 minutes).
 *
 * The "infinite lifetime" PMTU entry type is not implemented, since it is
 * useful only in an very limited number of cases. See the RFC for further
 * details.
 */

class Ipv4PmtuCache : public Object
{
public:
  class Entry;

  /**
   * \brief Get the type ID
   * \return type ID
   */
  static TypeId GetTypeId ();

  /**
   * \brief Constructor.
   */
  Ipv4PmtuCache ();

  /**
   * \brief Destructor.
   */
  ~Ipv4PmtuCache ();

  /**
   * \brief Dispose object.
   */
  virtual void DoDispose ();

  /**
   * \brief Gets the known Path MTU for the specific destination
   * \param dst the destination
   * \return the Path MTU (zero if unknown)
   */
  uint32_t GetPmtu (Ipv4Address dst);

  /**
   * \brief Sets the Path MTU for the specific destination
   * \param dst the destination
   * \param pmtu the Path MTU
   */
  void SetPmtu (Ipv4Address dst, uint32_t pmtu);

  /**
   * \brief Gets the Path MTU validity time
   * \return the Path MTU validity time
   */
  Time GetPmtuValidityTime () const;

  /**
   * \brief Sets the Path MTU validity time (minimum is 5 minutes)
   * \param validity the Path MTU validity time
   * \return true if the change was successful
   */
  bool SetPmtuValidityTime (Time validity);

  /**
   * \brief callback to notify pmtu change over IPv4
   */
  typedef Callback<void, Ipv4Address, uint32_t > PmtuChangeCallback;

  /**
   * \brief Set the callback to notify pmtu change over IPv4
   * \param callback the callback
   */
  void SetPmtuChangeCallback (PmtuChangeCallback callback);

private:
  /**
   * \brief Clears the Path MTU for the specific destination
   * \param dst the destination
   */
  void ClearPmtu (Ipv4Address dst);

  /**
   * \brief Path MTU table
   */
  std::map<Ipv4Address, uint32_t> m_pathMtu;

  /**
   * \brief Container of the Ipv4 PMTU data (Ipv4 destination address and expiration event).
   */
  typedef std::map<Ipv4Address, EventId>::iterator pathMtuTimerIter;

  /**
   * \brief Path MTU Expiration table
   */
  std::map<Ipv4Address, EventId> m_pathMtuTimer;

  /**
   * \brief Path MTU entry validity time
   */
  Time m_validityTime;

  /**
   * \brief Callback to notify pmtu change over IPv4
   */
  PmtuChangeCallback m_pmtuChangeCallback;
};

}

#endif /* Ipv4_PMTU_CACHE_H */
